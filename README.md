#安装
composer require chenzeming/qiniu_security  

#使用  
初始化配置  
QiniuSecurity::initAccount($ak,$sk);  


#检测图片  
QiniuSecurity::checkImage($imageUrlErr);  
未通过返回false  
未通过原因为  
QiniuSecurity::$err  


#检测文字  
QiniuSecurity::checkText('待检测文字');  
未通过返回false  
未通过原因为  
QiniuSecurity::$err  

#检测视频为 异步回调模式  
QiniuSecurity::setNotifyUrl('检测服务回调的URL');  

$videoUrl = '视频链接'  
$sn = '检测ID 非必填 会自动生成'  
$job_id =   checkVideo($videoUrl, $sn)  
这里会返回 $job_id  

#检测完成后会往回调地址发送信息  
读取回调信息处理  传入回调的信息 $result （传入数组）  
$status = videoNotifyHandle($result)  
$status 返回样例  
[  
    "job" => "63536828a2cfbcaec1aa7e81", //这里为checkVideo接口返回的job_id  
    "status" => false, //为true表示检测通过  
    "err" => "视频内容中包含 暴恐 信息"  
]  

